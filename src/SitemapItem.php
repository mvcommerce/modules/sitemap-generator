<?php

namespace MVCommerceModules\SitemapGenerator;


use Illuminate\Contracts\Support\Arrayable;

class SitemapItem implements Arrayable
{
    public $location, $lastModified, $changeFrequency = NULL, $priority = NULL;


    public function __construct($location, $lastModified = NULL, string $changeFrequency = NULL, float $priority = NULL)
    {
        $this->location = $location;
        $this->lastModified = $lastModified;
        $this->changeFrequency = $changeFrequency;
        $this->priority = $priority;
    }


    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $data = ['loc' => $this->location];

        /** @var \DateTime $lastModified */
        $lastModified = $this->lastModified ?? '';
        $lastModified = $lastModified instanceof \DateTime ? $lastModified->format('Y-m-d') : $lastModified;
        $lastModified = is_int($lastModified) ? now()->timestamp($lastModified)->format('Y-m-d') : $lastModified;

        if($this->lastModified) $data['lastmod'] = $lastModified;
        if($this->changeFrequency) $data['changefreq'] = $this->changeFrequency;
        if($this->priority) $data['priority'] = $this->priority;

        return $data;
    }

    public function __toString(): string
    {
        $string = '';
        foreach ($this->toArray() as $key => $value){
            $key = htmlentities($key);
            $value = htmlentities($value);
            $string .= sprintf("<%s>%s</%s>", $key, $value, $key);
        }

        return $string;
    }

}
