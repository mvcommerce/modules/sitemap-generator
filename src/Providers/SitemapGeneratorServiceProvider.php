<?php

namespace MVCommerceModules\SitemapGenerator\Providers;


use Illuminate\Support\ServiceProvider;

class SitemapGeneratorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->publishes([
            __DIR__.'/../../config/sitemap.php' => config_path('mvcommerce/sitemap.php'),
        ], 'config');

    }


    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__.'/../../config/sitemap.php', 'mvcommerce.sitemap'
        );

    }

}
