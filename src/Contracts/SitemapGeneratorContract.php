<?php

namespace MVCommerceModules\SitemapGenerator\Contracts;


use MVCommerceModules\SitemapGenerator\SitemapItem;

interface SitemapGeneratorContract
{
    public function name(): string;
    public function isComplete(): bool;
    public function next();
}
