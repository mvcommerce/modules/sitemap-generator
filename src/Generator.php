<?php

namespace MVCommerceModules\SitemapGenerator;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use MVCommerceModules\SitemapGenerator\Contracts\SitemapGeneratorContract;

class Generator
{

    public function filePath($name){
        $name = Str::slug($name);
        return public_path(config('mvcommerce.sitemap.public_dir') . '/' . $name . '.xml');
    }


    public function fileUrl($name){
        $name = Str::slug($name);
        return url(config('mvcommerce.sitemap.public_dir') . '/' . $name . '.xml');
    }


    protected function createSitemapDirectoryIfNotExists(){

        $dir = public_path(config('mvcommerce.sitemap.public_dir'));

        if(!File::isDirectory($dir)){
            File::makeDirectory($dir, 0755, true);
        }

    }

    public function generate(){

        $this->createSitemapDirectoryIfNotExists();

        $indexFile = $this->filePath('index');
        File::put($indexFile, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);

        $generators = config('mvcommerce.sitemap.generators') ?? [];
        foreach ($generators as $generator){
            if( class_exists($generator) ){
                try{
                    $generator = new $generator();

                    // Start processing the sitemap generator.
                    $this->process($generator);

                    // After completing the process add the sitemap url to index.
                    File::append($indexFile, "  <sitemap><loc>" . $this->fileUrl($generator->name()) . "</loc></sitemap>\n");

                }catch (\Exception $e){};
            }
        }

        File::append($indexFile, '</sitemapindex>');

    }


    protected function process(SitemapGeneratorContract $generator){

        $path = $this->filePath($generator->name());
        File::put($path, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);

        while (!$generator->isComplete()){
            $item = $generator->next();
            if(!$item) continue;
            File::append($path, "  <url>$item</url>" . PHP_EOL);
        }

        File::append($path, '</urlset>');
    }

}
