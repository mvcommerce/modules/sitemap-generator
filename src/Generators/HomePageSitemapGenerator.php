<?php

namespace MVCommerceModules\SitemapGenerator\Generators;


use MVCommerceModules\SitemapGenerator\Contracts\SitemapGeneratorContract;
use MVCommerceModules\SitemapGenerator\SitemapItem;

class HomePageSitemapGenerator implements SitemapGeneratorContract
{

    protected $complete = false;


    public function name(): string
    {
        return 'home';
    }

    public function isComplete(): bool
    {
        return $this->complete;
    }

    public function next(): SitemapItem
    {
        $this->complete = true;
        return new SitemapItem(
            url('/'),
            now()->subDays(1),
            'daily',
            1
        );
    }
}
