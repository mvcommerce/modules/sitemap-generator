<?php


return [



    'public_dir' => 'sitemaps',



    // Add classes the implements SitemapGeneratorContract

    'generators' => [
        \MVCommerceModules\SitemapGenerator\Generators\HomePageSitemapGenerator::class,
    ],
];
